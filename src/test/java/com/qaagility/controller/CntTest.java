package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CntTest {
  @Test
  public void testCnt() throws Exception {
  
  int k= new Cnt().d( 12, 4);
  assertEquals("Cnt", 3, k);
  }
  
  @Test
  public void testCntZero() throws Exception {
  
  int k= new Cnt().d( 12, 0);
  assertEquals("Cntzero", Integer.MAX_VALUE, k);
  }
}
